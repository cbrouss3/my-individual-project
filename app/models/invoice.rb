class Invoice < ActiveRecord::Base
  belongs_to :diagnostics
  belongs_to :appointment
  belongs_to :admin
  belongs_to :patient
  validate :patient, on: :create
  attr_protected :admin


  def active_patient
    errors.add(:patient_id, 'is not active') unless patient.active?
  end

end
