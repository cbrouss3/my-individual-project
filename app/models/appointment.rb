class Appointment < ActiveRecord::Base
  attr_accessible :person
  belongs_to :physician
  belongs_to :patient
  belongs_to :doctors_office
  has_many :invoices
  has_many :diagnostics, through: :invoices
  attr_protected :admin

  def person
    @patients = Patient.all
  end
  def doctor
    @physicians = Physician.all
  end
end