class Diagnostic < ActiveRecord::Base
  has_many :invoices
  has_many :appointments, through: :invoices
  belongs_to :admin
  attr_protected :admin

end
