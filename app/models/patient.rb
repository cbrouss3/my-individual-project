class Patient < ActiveRecord::Base
  has_many :appointments
  has_many :physicians, through: :appointments
  attr_protected :admin

  def full_name
    "#{lastname} #{firstname}"

  end
  def patient_id
    "#{id} #{lastname} #{firstname}"
  end
end



