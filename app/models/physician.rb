class Physician < ActiveRecord::Base
  has_many :appointments
  has_many :patients, through: :appointments
  belongs_to :Admin
  attr_protected :admin


  def full_name
    "#{firstname} #{lastname}"
  end

end
