class DoctorsOffice < ActiveRecord::Base
  has_many :invoices
  has_many :appointments, through: :invoices
  attr_protected :admin

end
