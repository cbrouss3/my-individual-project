class DoctorsOfficesController < ApplicationController
  # GET /doctors_offices
  # GET /doctors_offices.json
  def index
    @doctors_offices = DoctorsOffice.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @doctors_offices }
    end
  end

  # GET /doctors_offices/1
  # GET /doctors_offices/1.json
  def show
    @doctors_office = DoctorsOffice.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @doctors_office }
    end
  end

  # GET /doctors_offices/new
  # GET /doctors_offices/new.json
  def new
    @doctors_office = DoctorsOffice.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @doctors_office }
    end
  end

  # GET /doctors_offices/1/edit
  def edit
    @doctors_office = DoctorsOffice.find(params[:id])
  end

  # POST /doctors_offices
  # POST /doctors_offices.json
  def create
    @doctors_office = DoctorsOffice.new(params[:doctors_office])

    respond_to do |format|
      if @doctors_office.save
        format.html { redirect_to @doctors_office, notice: 'Doctors office was successfully created.' }
        format.json { render json: @doctors_office, status: :created, location: @doctors_office }
      else
        format.html { render action: "new" }
        format.json { render json: @doctors_office.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /doctors_offices/1
  # PUT /doctors_offices/1.json
  def update
    @doctors_office = DoctorsOffice.find(params[:id])

    respond_to do |format|
      if @doctors_office.update_attributes(params[:doctors_office])
        format.html { redirect_to @doctors_office, notice: 'Doctors office was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @doctors_office.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /doctors_offices/1
  # DELETE /doctors_offices/1.json
  def destroy
    @doctors_office = DoctorsOffice.find(params[:id])
    @doctors_office.destroy

    respond_to do |format|
      format.html { redirect_to doctors_offices_url }
      format.json { head :no_content }
    end
  end
end
