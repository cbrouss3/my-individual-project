require 'test_helper'

class DoctorsOfficesControllerTest < ActionController::TestCase
  setup do
    @doctors_office = doctors_offices(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:doctors_offices)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create doctors_office" do
    assert_difference('DoctorsOffice.count') do
      post :create, doctors_office: { name: @doctors_office.name, officetitle: @doctors_office.officetitle }
    end

    assert_redirected_to doctors_office_path(assigns(:doctors_office))
  end

  test "should show doctors_office" do
    get :show, id: @doctors_office
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @doctors_office
    assert_response :success
  end

  test "should update doctors_office" do
    put :update, id: @doctors_office, doctors_office: { name: @doctors_office.name, officetitle: @doctors_office.officetitle }
    assert_redirected_to doctors_office_path(assigns(:doctors_office))
  end

  test "should destroy doctors_office" do
    assert_difference('DoctorsOffice.count', -1) do
      delete :destroy, id: @doctors_office
    end

    assert_redirected_to doctors_offices_path
  end
end
