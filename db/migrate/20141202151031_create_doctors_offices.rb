class CreateDoctorsOffices < ActiveRecord::Migration
  def change
    create_table :doctors_offices do |t|
      t.string :name
      t.string :officetitle

      t.timestamps
    end
  end
end
