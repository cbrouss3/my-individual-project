class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.integer :diag_code
      t.integer :appoint_id
      t.integer :admin_id

      t.timestamps
    end
  end
end
