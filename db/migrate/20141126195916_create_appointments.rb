class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.datetime :appointment
      t.integer :patient_id
      t.integer :physician_id

      t.timestamps
    end
  end
end
