class CreateHomes < ActiveRecord::Migration
  def change
    create_table :homes do |t|
      t.string :date
      t.string :hour

      t.timestamps
    end
  end
end
