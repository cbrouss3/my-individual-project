# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Physician.create(lastname: 'Broussard', firstname: 'Christopher')
Physician.create(lastname: 'Thomas', firstname: 'Brandy')
Physician.create(lastname: 'Hernandez', firstname: 'Joe')

Patient.create(lastname: 'Sims', firstname: 'Colton', problem: 'Ebola Symptoms')
Patient.create(lastname: 'James', firstname: 'Rick', problem: 'Std')
Patient.create(lastname: 'Williams', firstname: 'Stacy', problem: 'The Flu')
Patient.create(lastname: 'David', firstname: 'Sammy', problem: 'Hiv')
Patient.create(lastname: 'Banks', firstname: 'Carlton', problem: 'Prostate Exam')
Patient.create(lastname: 'Adams', firstname: 'Tim', problem: 'Hiv Tets')
Patient.create(lastname: 'Mckay', firstname: 'Cara', problem: 'Abortion')
Patient.create(lastname: 'Bryan', firstname: 'Billy', problem: 'Broken Leg')
Patient.create(lastname: 'Powers', firstname: 'Austin', problem: 'Flu Shot')
Patient.create(lastname: 'Simpson', firstname: 'Homer', problem: 'Head Trauma')

Diagnostic.create(code: '919.0', description: 'Abrasion without infection', price: '10.0')
Diagnostic.create(code: '682.9 ', description: ' Abscess, acute NOS', price: '85.0')
Diagnostic.create(code: '706.1', description: 'Abrasion without infection', price: '152.0')
Diagnostic.create(code: '692.3 ', description: 'Acneiform drug eruption', price: '100.0')
Diagnostic.create(code: '701.9', description: 'Achrochordon', price: '152.55')
Diagnostic.create(code: '216.7', description: 'Achrochordon', price: '123.55')
Diagnostic.create(code: '702._*', description: 'Actinic keratosis', price: '65.75')
Diagnostic.create(code: '112.0', description: 'Thrush', price: '20.0')
Diagnostic.create(code: '110.5 ', description: ' Tinea corporis', price: '25.0')
Diagnostic.create(code: '110.3', description: 'Tinea cruris', price: '19.0')
Diagnostic.create(code: '078.19 ', description: 'Verruca, Plana', price: '99.0')
Diagnostic.create(code: '706.8', description: 'Xerosis', price: '140.55')
Diagnostic.create(code: '272.2', description: 'Xanthoma, NOS', price: '200.55')
Diagnostic.create(code: '447.6', description: 'Vasculitis', price: '50.85')







